from django.test import TestCase


# Create your tests here.
class UserTestCase(TestCase):
    def setUp(self):
        '''This should never execute but it does when I test test_store_a'''
        self.data = None

    '''Test create, update and display information staff'''
    def test_staff(self):
        staff_id = ''
        """
        Test create information staff
        """
        print('\nCreate information staff!')
        with open('/home/dat/Downloads/0812_wp4676582-4k-pc-wallpapers.jpg', 'rb') as img:
            data_staff = {
                'name': 'Nguyen Van Dat',
                'gender': 'male',
                'avatar': img
            }

            response = self.client.post('/staff/submit', data_staff)

            print(response.status_code)
            print(response.context['staff'])
            staff_id = response.context['staff']['id']
            self.assertEqual(response.status_code, 200)

        """
        Test update information staff
        """
        print('\nUpdate information staff!')
        with open('/home/dat/Downloads/0812_wp4676582-4k-pc-wallpapers.jpg', 'rb') as img:
            data_staff = {
                'name': 'Nguyen Van Hung',
                'gender': 'female',
                'avatar': img
            }

            response = self.client.post(
                '/staff/update/' + str(staff_id), data_staff)

            print(response.status_code)
            print(response.context['staff'])
            self.assertEqual(response.status_code, 200)

        """
        Test update information staff with invalid name!
        """
        print('\nUpdate information staff with invalid name!')
        with open('/home/dat/Downloads/0812_wp4676582-4k-pc-wallpapers.jpg', 'rb') as img:
            data_staff = {
                'name': '<script></script>',
                'gender': 'female',
                'avatar': img
            }

            response = self.client.post(
                '/staff/update/' + str(staff_id), data_staff)

            print(response.status_code)
            print(response.context['message'])
            self.assertEqual(response.status_code, 400)

        """
        Test update information staff with invalid name 2!
        """
        print('\nUpdate information staff with invalid name 2!')
        with open('/home/dat/Downloads/0812_wp4676582-4k-pc-wallpapers.jpg', 'rb') as img:
            data_staff = {
                'name': '" or ""="',
                'gender': 'female',
                'avatar': img
            }

            response = self.client.post(
                '/staff/update/' + str(staff_id), data_staff)

            print(response.status_code)
            print(response.context['message'])
            self.assertEqual(response.status_code, 400)

        """
        Test update information staff with invalid avatar!
        """
        print('\nUpdate information staff with invalid avatar!')
        with open('/home/dat/Documents/BaoCao/bao-cao-cong-viec-14-12.odt', 'rb') as img:
            data_staff = {
                'name': 'Nguyen Van Hung',
                'gender': 'female',
                'avatar': img
            }

            response = self.client.post(
                '/staff/update/' + str(staff_id), data_staff)

            print(response.status_code)
            print(response.context['message'])
            self.assertEqual(response.status_code, 400)

        """
        Display all staff
        """
        print('\nDisplay information staff!')
        response = self.client.get(
            '/staff/info/' + str(staff_id))

        print(response.status_code)
        print(response.context['staff'])
        self.assertEqual(response.status_code, 200)

        """
        Display information staff update
        """
        print('\nDisplay information staff update!')
        response = self.client.get(
            '/staff/info_update/' + str(staff_id))

        print(response.status_code)
        print(response.context['staff'])
        self.assertEqual(response.status_code, 200)

    '''Test create information staff not name!'''
    def test_create_staff_not_name(self):
        print('\nCreate information staff not name!')
        with open('/home/dat/Downloads/0812_wp4676582-4k-pc-wallpapers.jpg', 'rb') as img:
            data_staff = {
                'gender': 'male',
                'avatar': img
            }

            response = self.client.post('/staff/submit', data_staff)

            print(response.status_code)
            print(response.context['message'])
            self.assertEqual(response.status_code, 400)

    '''Test create information staff invalid name'''
    def test_create_staff_name_invalid(self):
        print('\nCreate information staff with invalid name!')
        with open('/home/dat/Downloads/0812_wp4676582-4k-pc-wallpapers.jpg', 'rb') as img:
            data_staff = {
                'name': '<script></script>',
                'gender': 'female',
                'avatar': img
            }

            response = self.client.post('/staff/submit', data_staff)

            print(response.status_code)
            print(response.context['message'])
            self.assertEqual(response.status_code, 400)

    '''Test create information staff invalid name 2'''
    def test_create_staff_name_invalid_2(self):
        print('\nCreate information staff with invalid name 2!')
        with open('/home/dat/Downloads/0812_wp4676582-4k-pc-wallpapers.jpg', 'rb') as img:
            data_staff = {
                'name': '" or ""="',
                'gender': 'female',
                'avatar': img
            }

            response = self.client.post('/staff/submit', data_staff)

            print(response.status_code)
            print(response.context['message'])
            self.assertEqual(response.status_code, 400)

    '''Test create information staff not gender'''
    def test_create_staff_not_gender(self):
        print('\nCreate information staff not gender!')
        with open('/home/dat/Downloads/0812_wp4676582-4k-pc-wallpapers.jpg', 'rb') as img:
            data_staff = {
                'name': 'Nguyen Van Dat',
                'avatar': img
            }

            response = self.client.post('/staff/submit', data_staff)

            print(response.status_code)
            print(response.context['message'])
            self.assertEqual(response.status_code, 400)

    '''Test create information staff not avatar'''
    def test_create_staff_not_avatar(self):
        print('\nCreate information staff not avatar!')
        data_staff = {
            'name': 'Nguyen Van Dat',
            'gender': 'male'
        }

        response = self.client.post('/staff/submit', data_staff)

        print(response.status_code)
        print(response.context['message'])
        self.assertEqual(response.status_code, 400)

    '''Test create information staff invalid avatar'''
    def test_create_staff_with_invalid_avatar(self):
        print('\nCreate information staff with invalid avatar!')
        with open('/home/dat/Documents/BaoCao/bao-cao-cong-viec-14-12.odt', 'rb') as img:
            data_staff = {
                'name': 'Nguyen Van Hung',
                'gender': 'female',
                'avatar': img
            }

            response = self.client.post('/staff/submit', data_staff)

            print(response.status_code)
            print(response.context['message'])
            self.assertEqual(response.status_code, 400)

    '''Test update information staff wrong id'''
    def test_update_staff_wrong_id(self):
        print('\nUpdate information staff with staff id is wrong!')
        with open('/home/dat/Downloads/0812_wp4676582-4k-pc-wallpapers.jpg', 'rb') as img:
            data_staff = {
                'name': 'Nguyen Van Hung',
                'gender': 'female',
                'avatar': img
            }

            staff_id = '8c8477234f384c55a53acf7403236846'

            response = self.client.post(
                '/staff/update/' + str(staff_id), data_staff)

            print(response.status_code)
            print(response.context['message'])
            self.assertEqual(response.status_code, 400)

    '''Test display information staff wrong id'''
    def test_display_info_staff_with_wrong_id(self):
        print('\n Display information staff update with staff id is empty!')
        staff_id = '8c8477234f384c55a53acf7403236846'

        response = self.client.get(
            '/staff/info_update/' + str(staff_id))

        print(response.status_code)
        self.assertEqual(response.status_code, 400)

    '''Test display all log'''
    def test_show_log(self):
        print('\n Display all log!')
        response = self.client.get('/staff/log')
        print(response.status_code)
        self.assertEqual(response.status_code, 200)
