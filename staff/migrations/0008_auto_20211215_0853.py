# Generated by Django 3.2.10 on 2021-12-15 08:53

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('staff', '0007_auto_20211215_0850'),
    ]

    operations = [
        migrations.AlterField(
            model_name='log',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2021, 12, 15, 8, 53, 35, 909608)),
        ),
        migrations.AlterField(
            model_name='user',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2021, 12, 15, 8, 53, 35, 909301)),
        ),
    ]
