# Generated by Django 3.2.10 on 2021-12-15 08:16

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('staff', '0005_auto_20211214_0849'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='log',
            name='method',
        ),
        migrations.AlterField(
            model_name='log',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2021, 12, 15, 8, 16, 6, 857239)),
        ),
        migrations.AlterField(
            model_name='user',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2021, 12, 15, 8, 16, 6, 856969)),
        ),
    ]
