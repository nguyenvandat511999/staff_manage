from rest_framework import serializers


class UserSerializers(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    gender = serializers.CharField(max_length=100)


class AvatarSerializers(serializers.Serializer):
    avatar = serializers.FileField()
