from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('submit', views.submit, name='submit'),
    path('update/<staff_id>', views.update, name='update'),
    path('list/', views.display_all_staff, name='list'),
    path('info/<staff_id>', views.show_info, name='show'),
    path('info_update/<staff_id>', views.info_update, name='info'),
    path('log', views.display_log, name='log'),
]
