# from .views import allowed_file
from . import serializers


def validate_data(request, type):
    """
    Check input data is valid
    """
    validation_data = serializers.UserSerializers(data=request.POST)

    if not validation_data.is_valid():
        message = {
            'message': 'Invalid data!'
        }
        return message

    if type == 'create':
        validation_avatar = serializers.AvatarSerializers(data=request.FILES)
        if not validation_avatar.is_valid():
            message = {
                'message': 'Invalid avatar!'
            }
            return message

    elif type == 'update':
        # if request.FILES is not None:
        #     validation_avatar = serializers.AvatarSerializers(data=request.FILES)
        #     if (not validation_avatar.is_valid()) or (not allowed_file(request.FILES.name)):
        #         message = {
        #             'message': 'Invalid avatar!'
        #         }
        #         return message

        return None
