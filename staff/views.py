import os
import re

from django.db import transaction
from django.shortcuts import render
from django.core.files.storage import FileSystemStorage
from staff_app.settings import ALLOWED_EXTENSIONS, BASE_DIR

from . import serializers
from .models import Log, User


def allowed_file(filename):
    """
    Function check file
    """
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def format_data_staff(record_staff):
    """
    Function to format staff data to return
    """
    staff = {
        'staff': {
            'id': record_staff.id,
            'name': record_staff.name,
            'gender': record_staff.gender,
            'avatar': record_staff.avatar
        }
    }

    return staff


def upload_file(record_staff, file):
    """
    Function upload file avatar
    """
    staff_folder = 'media/' + str(record_staff.id)

    path_url = os.path.join(BASE_DIR, staff_folder)

    media_url = '/staff/media/' + str(record_staff.id)

    file_url = ''
    if file and allowed_file(file.name):
        record_staff = User()

        fss = FileSystemStorage(location=path_url, base_url=media_url)
        file = fss.save(file.name, file)
        file_url = fss.url(file)

    return file_url


def validate_data(request, type):
    """
    Check input data is valid
    """
    validation_data = serializers.UserSerializers(data=request.POST)

    if not validation_data.is_valid():
        message = {
            'message': 'Invalid data!'
        }
        return message

    if type == 'create':
        validation_avatar = serializers.AvatarSerializers(data=request.FILES)
        if not validation_avatar.is_valid():
            message = {
                'message': 'Invalid avatar!'
            }
            return message

    elif type == 'update':
        if request.FILES is not None:
            validation_avatar = serializers.AvatarSerializers(
                data=request.FILES)
            if (not validation_avatar.is_valid()) or (not allowed_file(request.FILES.get('avatar').name)):
                message = {
                    'message': 'Invalid avatar!'
                }
                return message

        return None


def index(request):
    """
    Display form add staff information
    """
    index_url = str(BASE_DIR) + '/staff/template/user/info.html'
    return render(request, index_url, status=200)


@transaction.atomic
def submit(request):
    """
    Function add staff information
    """
    info_url = str(BASE_DIR) + '/staff/template/user/info.html'

    name = request.POST.get('name')
    gender = request.POST.get('gender')
    avatar = request.FILES.get('avatar')

    """
    Check special characters in name
    """
    name_check = re.compile('[@_!#$%^&*(),.;''""<>?/\|}{~:=+]')
    if name and name_check.search(name) is not None:
        return render(request, info_url, {'message': 'Invalid name!'}, status=400)

    """
    Check data is valid
    """
    validation_data = validate_data(request, 'create')
    if validation_data:
        return render(request, info_url, validation_data, status=400)

    record_staff = User()
    file_url = upload_file(record_staff, avatar)

    if file_url:
        record_staff.create_update_user(name, gender, file_url)

        staff = format_data_staff(record_staff)

        record_log = Log().create_update_log('Create new staff information!', 'Staff')

        return render(request, info_url, staff, status=200)

    return render(request, info_url, {'message': 'Create information failed!'}, status=400)


@transaction.atomic
def update(request, staff_id):
    """
    Function update staff information
    """
    info_url = str(BASE_DIR) + '/staff/template/user/info.html'

    """
    Query staff and check staff exist
    """
    record_staff = User.objects.filter(id=staff_id).first()
    if not record_staff:
        return render(request, info_url, {'message': 'User does not exist!'}, status=400)
    staff = format_data_staff(record_staff)

    name = request.POST.get('name') if request.POST.get('name') is not None else record_staff.name
    gender = request.POST.get('gender') if request.POST.get('gender') is not None else record_staff.gender
    avatar = request.FILES.get('avatar')

    """
    Check special characters in name
    """
    name_check = re.compile('[@_!#$%^&*(),.;''""<>?/\|}{~:=+]')
    if name and name_check.search(name) is not None:
        index_url = str(BASE_DIR) + '/staff/template/user/update.html'
        message = {
            'message': 'Invalid name!',
            'staff': staff['staff']
        }
        return render(request, index_url, message, status=400)

    """
    Check data is valid
    """
    validation_data = validate_data(request, 'update')
    if validation_data:
        index_url = str(BASE_DIR) + '/staff/template/user/update.html'
        validation_data['staff'] = staff['staff']
        return render(request, index_url, validation_data, status=400)

    file_url = ''
    if avatar:
        file_url = upload_file(record_staff, avatar)

    avatar = file_url if file_url else record_staff.avatar

    record_staff.create_update_user(name, gender, avatar)

    staff = format_data_staff(record_staff)

    record_log = Log().create_update_log('Update staff information!', 'Staff')

    return render(request, info_url, staff, status=200)


def display_all_staff(request):
    """
    Function display all staff information
    """
    list_staff_url = str(BASE_DIR) + '/staff/template/user/list_staff.html'

    records_staff = User.objects.all()

    data_staff = []
    for data_record in records_staff:
        if data_record:
            data = {
                'id': data_record.id,
                'name': data_record.name,
                'gender': data_record.gender,
                'avatar': data_record.avatar
            }
            data_staff.append(data)

    staff = {
        'staff': data_staff
    }

    record_log = Log().create_update_log(
        'Display all staff information!', 'Staff')

    return render(request, list_staff_url, staff, status=200)


def show_info(request, staff_id):
    """
    Function display staff information by id
    """
    index_url = str(BASE_DIR) + '/staff/template/user/index.html'

    record_staff = User.objects.filter(id=staff_id).first()

    if not record_staff:
        return render(request, index_url, {'message': 'User does not exist!'}, status=400)

    staff = format_data_staff(record_staff)

    record_log = Log().create_update_log(
        'Complete create staff information!', 'Staff')

    return render(request, index_url, staff, status=200)


def info_update(request, staff_id):
    """
    Function display staff information that needs to be edited
    """
    index_url = str(BASE_DIR) + '/staff/template/user/update.html'

    if not staff_id:
        info_url = str(BASE_DIR) + '/staff/template/user/info.html'
        return render(request, info_url, {'message': 'Not found!'}, status=404)

    record_staff = User.objects.filter(id=staff_id).first()

    if not record_staff:
        return render(request, index_url, {'message': 'User does not exist!'}, status=400)

    staff = format_data_staff(record_staff)

    record_log = Log().create_update_log('Display staff information update!', 'Staff')

    return render(request, index_url, staff, status=200)


def display_log(request):
    """
    Function display all log
    """
    log_url = str(BASE_DIR) + '/staff/template/log/index.html'

    records_log = Log.objects.all()

    data_log = []
    if records_log:
        for data in records_log:
            data = {
                'content': data.content,
                'time': data.created_at,
                'model': data.model
            }

            data_log.append(data)

    log = {
        'log': data_log
    }

    record_log = Log().create_update_log('Display all log!', 'Log')

    return render(request, log_url, log, status=200)
